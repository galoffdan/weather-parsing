import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

public class Main {
    public static void main(String[] args) throws IOException {
        Main main = new Main();
//        main.createSmth("file.txt");
        main.breadthSearch("/home/miguelius");
    }

    private void breadthSearch(String path) {
        File file = new File(path);
        String ans;
        if (file.isDirectory()) {
            ans = goBreadth(file.listFiles());
        } else {
            ans = goBreadth(file.getParentFile().listFiles());
        }
        System.out.println(ans);
    }

    private String goBreadth(File[] files) {
        Queue<File> fileQueue = new ArrayDeque<>(Arrays.asList(files));
        StringBuilder ans = new StringBuilder();
        StringBuilder filesNames = new StringBuilder();
        while (fileQueue.peek() != null) {
            File parent = fileQueue.poll();
            ans.append(parent.getName() + " ");
            if (parent.isDirectory()) {
                String[] list = parent.list();
                for (int i = 0; i < list.length; i++) {
                    filesNames.append(list[i] + " ");
                }
            }
        }
        return ans.append(filesNames).toString();
    }

    private void createSmth(String path) throws IOException {
        File file = new File(path);
        if (file.isFile()) {
            String[] split = file.getName().split("\\.");
            file = new File(split[0]);
            file.mkdir();
        } else if (file.isDirectory()) {
            file = new File(file, path);
            file.createNewFile();
        }
    }
}
