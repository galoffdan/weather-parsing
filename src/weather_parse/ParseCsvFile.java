package weather_parse;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

enum WindDirection {
    N, NW, NE, S, SW, SE, E, W
}
public class ParseCsvFile {
    double averageWindSpeed, averageTemperature, averageHumidity;
    double highestTemperature = Double.MIN_VALUE;
    double lowestHumidity = Double.MAX_VALUE;
    double highestWindSpeed = Double.MIN_VALUE;
    Date highestTempDay, lowestHumDay, highestWindDay;
    WindDirection mostFrequentWindDirection;
    int[] directionsCount = new int[8];

    public static void main(String[] args) throws IOException {
        ParseCsvFile parseCsvFile = new ParseCsvFile();
        String csvSource = "/home/miguelius/Загрузки/Telegram Desktop/dataexport_20210320T064822.csv";
        String outputFile = "/home/miguelius/Загрузки/Telegram Desktop/weather";
        parseCsvFile.parse(csvSource);
        parseCsvFile.write(outputFile);
    }

    public void parse(String path) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            while (!(reader.readLine().split(",")[0].equals("timestamp"))) {
            }
            String str;
            int countOfHours = 0;
            while ((str = reader.readLine()) != null) {
                parseString(str.split(","));
                countOfHours++;
            }
            countFrequentDirection(directionsCount);
            averageHumidity /= countOfHours;
            averageTemperature /= countOfHours;
            averageWindSpeed /= countOfHours;
            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("File doesn't exist. Please, write correct path");
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private void parseString(String[] data) throws ParseException {
        double currTemp = Double.parseDouble(data[1]);
        double currHumidity = Double.parseDouble(data[2]);
        double currWindSpeed = Double.parseDouble(data[3]);
        double windDirection = Double.parseDouble(data[4]);
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");
        if (windDirection >= 337.5 && windDirection < 22.5) {
            directionsCount[0]++;
        }
        if (windDirection >= 22.5 && windDirection < 67.5) {
            directionsCount[1]++;
        }
        if (windDirection >= 67.5 && windDirection < 112.5) {
            directionsCount[2]++;
        }
        if (windDirection >= 112.5 && windDirection < 157.5) {
            directionsCount[3]++;
        }
        if (windDirection >= 157.5 && windDirection < 202.5) {
            directionsCount[4]++;
        }
        if (windDirection >= 202.5 && windDirection < 247.5) {
            directionsCount[5]++;
        }
        if (windDirection >= 247.5 && windDirection < 292.5) {
            directionsCount[6]++;
        }
        if (windDirection >= 292.5 && windDirection < 337.5) {
            directionsCount[7]++;
        }
        averageTemperature += currTemp;
        averageHumidity += currHumidity;
        averageWindSpeed += currWindSpeed;
        String[] ts = data[0].split("T");
        Date currDay = format.parse(ts[0] + ts[1]);
        if (highestTemperature < currTemp) {
            highestTemperature = currTemp;
            highestTempDay = currDay;
        }
        if (lowestHumidity > currHumidity) {
            lowestHumidity = currHumidity;
            lowestHumDay = currDay;
        }
        if (currWindSpeed > highestWindSpeed) {
            highestWindSpeed = currWindSpeed;
            highestWindDay = currDay;
        }
    }

    private void countFrequentDirection(int[] directionsCount) {
        int indexMaxCount = 0;
        int maxCount = 0;
        for (int i = 0; i < directionsCount.length; i++) {
            if (directionsCount[i] > maxCount) {
                maxCount = directionsCount[i];
                indexMaxCount = i;
            }
        }
        switch (indexMaxCount) {
            case 0:
                mostFrequentWindDirection = WindDirection.N;
                break;
            case 1:
                mostFrequentWindDirection = WindDirection.NE;
                break;
            case 2:
                mostFrequentWindDirection = WindDirection.E;
                break;
            case 3:
                mostFrequentWindDirection = WindDirection.SE;
                break;
            case 4:
                mostFrequentWindDirection = WindDirection.S;
                break;
            case 5:
                mostFrequentWindDirection = WindDirection.SW;
                break;
            case 6:
                mostFrequentWindDirection = WindDirection.W;
                break;
            case 7:
                mostFrequentWindDirection = WindDirection.NW;
        }
    }
    public void write (String path) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(String.format(Locale.ENGLISH,
                "Average humidity: %.2f\n" +
                "Average temperature: %.2f\n" +
                "Average wind speed: %.2f\n" +
                "Max temperature: %.2f was on %td %tB %tH:00\n" +
                "Lowest humidity: %.2f was on %td %tB %tH:00\n" +
                "Highest wind speed: %.2f was on %td %tB %tH:00\n" +
                "Most frequent wind direction is %s", averageHumidity, averageTemperature, averageWindSpeed,
                highestTemperature, highestTempDay, highestTempDay, highestTempDay,
                lowestHumidity, lowestHumDay, lowestHumDay, lowestHumDay,
                highestWindSpeed, highestWindDay, highestWindDay, highestWindDay,
                mostFrequentWindDirection));
        writer.close();
    }
}
